package org.codingdojo.yatzy;

import java.util.Collections;

public class Yatzy {

    private static final int SCORE_YATZY = 50;
    private static final int SCORE_ZERO = 0;
    private static final int SCORE_SMALL_STRAIGHT = 15;
    private static final int SCORE_LARGE_STRAIGHT = 20;

    public int score(Roll roll, Category category) {
        return switch (category) {
            case ONES -> ones(roll);
            case TWOS -> twos(roll);
            case THREES -> threes(roll);
            case FOURS -> fours(roll);
            case FIVES -> fives(roll);
            case SIXES -> sixes(roll);
            case ONE_PAIR -> onePair(roll);
            case TWO_PAIRS -> twoPairs(roll);
            case THREE_OF_A_KIND -> threeOfAKind(roll);
            case FOUR_OF_A_KIND -> fourOfAKind(roll);
            case SMALL_STRAIGHT -> smallStraight(roll);
            case LARGE_STRAIGHT -> largeStraight(roll);
            case FULL_HOUSE -> fullHouse(roll);
            case CHANCE -> chance(roll);
            case YATZY -> yatzy(roll);
        };
    }

    private int chance(Roll roll) {
        return roll.sum();
    }

    private int yatzy(Roll roll) {
        if (roll.isYatzy()) {
            return SCORE_YATZY;
        }
        return SCORE_ZERO;
    }

    private int ones(Roll roll) {
        return roll.sumOfDiceWithNumber(1);
    }

    private int twos(Roll roll) {
        return roll.sumOfDiceWithNumber(2);
    }

    private int threes(Roll roll) {
        return roll.sumOfDiceWithNumber(3);
    }

    private int fours(Roll roll) {
        return roll.sumOfDiceWithNumber(4);
    }

    private int fives(Roll roll) {
        return roll.sumOfDiceWithNumber(5);
    }

    private int sixes(Roll roll) {
        return roll.sumOfDiceWithNumber(6);
    }

    private int onePair(Roll roll) {
        return numberOfPairs(roll, 1);
    }

    private int twoPairs(Roll roll) {
        return numberOfPairs(roll, 2);
    }

    private int numberOfPairs(Roll roll, int numberOfPairs) {
        var diceAndFrequencies = roll.mapDiceAndFrequency();

        var scoresPairsDesc = diceAndFrequencies.entrySet().stream()
            .filter(diceAndFrequency -> diceAndFrequency.getValue() >= 2)
            .map(diceAndFrequency -> diceAndFrequency.getKey() * 2)
            .sorted(Collections.reverseOrder())
            .toList();

        if (scoresPairsDesc.size() >= numberOfPairs) {
            return scoresPairsDesc.stream()
                .limit(numberOfPairs)
                .mapToInt(Integer::intValue)
                .sum();
        }

        return SCORE_ZERO;
    }

    private int threeOfAKind(Roll roll) {
        return numberOfAKind(roll, 3);
    }

    private int fourOfAKind(Roll roll) {
        return numberOfAKind(roll, 4);
    }

    private int numberOfAKind(Roll roll, int number) {
        var diceAndFrequencies = roll.mapDiceAndFrequency();

        return diceAndFrequencies.entrySet().stream()
            .filter(diceAndFrequency -> diceAndFrequency.getValue() >= number)
            .map(diceAndFrequency -> diceAndFrequency.getKey() * number)
            .findFirst()
            .orElse(SCORE_ZERO);
    }

    private int smallStraight(Roll roll) {
        if (roll.isStraightAndDoesNotContains(6)) {
            return SCORE_SMALL_STRAIGHT;
        }
        return SCORE_ZERO;
    }

    private int largeStraight(Roll roll) {
        if (roll.isStraightAndDoesNotContains(1)) {
            return SCORE_LARGE_STRAIGHT;
        }
        return SCORE_ZERO;
    }

    private int fullHouse(Roll roll) {
        if (roll.isFullHouse()) {
            return roll.sum();
        }
        return SCORE_ZERO;
    }
}
