package org.codingdojo.yatzy;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Roll {

    private static final int SIZE = 5;
    private List<Integer> dice;

    public Roll(List<Integer> dice) {
        if (dice == null || dice.size() != SIZE) {
            throw new RollSizeException();
        }
        this.dice = dice;
    }

    int sum() {
        return dice.stream()
            .mapToInt(Integer::intValue)
            .sum();
    }

    int sumOfDiceWithNumber(int diceNumber) {
        return dice.stream()
            .filter(d -> d == diceNumber)
            .mapToInt(Integer::intValue).sum();
    }

    Map<Integer, Long> mapDiceAndFrequency() {
        return dice.stream()
            .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

    boolean isStraightAndDoesNotContains(int diceNumber) {
        return distinctSize() == 5 && !dice.contains(diceNumber);
    }

    boolean isYatzy() {
        return distinctSize() == 1;
    }

    boolean isFullHouse() {
        return distinctSize() == 2;
    }

    private int distinctSize() {
        return dice.stream()
            .distinct()
            .toList()
            .size();
    }
}
