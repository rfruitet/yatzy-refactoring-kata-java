package org.codingdojo.yatzy;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;

class RollTest {

    @Test
    void should_throw_roll_size_exception() {
        assertThrows(RollSizeException.class, () -> new Roll(List.of(1)));
        assertThrows(RollSizeException.class, () -> new Roll(List.of(1, 2, 3, 4, 5, 6, 7)));
        assertThrows(RollSizeException.class, () -> new Roll(null));
    }
}