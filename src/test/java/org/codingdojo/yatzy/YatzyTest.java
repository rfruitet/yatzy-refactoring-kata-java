package org.codingdojo.yatzy;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.codingdojo.yatzy.Category.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class YatzyTest {

    private final Yatzy yatzy = new Yatzy();

    @Test
    void should_test_category_chance() {
        assertEquals(15, yatzy.score(buildRoll(List.of(2, 3, 4, 5, 1)), CHANCE));
        assertEquals(16, yatzy.score(buildRoll(List.of(3, 3, 4, 5, 1)), CHANCE));
    }

    @Test
    void should_test_category_yatzy() {
        assertEquals(50, yatzy.score(buildRoll(List.of(4, 4, 4, 4, 4)), YATZY));
        assertEquals(50, yatzy.score(buildRoll(List.of(6, 6, 6, 6, 6)), YATZY));
        assertEquals(0, yatzy.score(buildRoll(List.of(6, 6, 6, 6, 3)), YATZY));
    }

    @Test
    void should_test_category_ones() {
        assertEquals(1, yatzy.score(buildRoll(List.of(1, 2, 3, 4, 5)), ONES));
        assertEquals(2, yatzy.score(buildRoll(List.of(1, 2, 1, 4, 5)), ONES));
        assertEquals(0, yatzy.score(buildRoll(List.of(6, 2, 2, 4, 5)), ONES));
        assertEquals(4, yatzy.score(buildRoll(List.of(1, 2, 1, 1, 1)), ONES));
    }

    @Test
    void should_test_category_twos() {
        assertEquals(4, yatzy.score(buildRoll(List.of(1, 2, 3, 2, 6)), TWOS));
        assertEquals(10, yatzy.score(buildRoll(List.of(2, 2, 2, 2, 2)), TWOS));
        assertEquals(0, yatzy.score(buildRoll(List.of(3, 4, 5, 6, 1, 4)), TWOS));
    }

    @Test
    void should_test_category_threes() {
        assertEquals(6, yatzy.score(buildRoll(List.of(1, 2, 3, 2, 3)), THREES));
        assertEquals(12, yatzy.score(buildRoll(List.of(2, 3, 3, 3, 3)), THREES));
        assertEquals(0, yatzy.score(buildRoll(List.of(1, 2, 5, 5, 4, 6)), THREES));
    }

    @Test
    void should_test_category_fours() {
        assertEquals(12, yatzy.score(buildRoll(List.of(4, 4, 4, 5, 5)), FOURS));
        assertEquals(8, yatzy.score(buildRoll(List.of(4, 4, 5, 5, 5)), FOURS));
        assertEquals(4, yatzy.score(buildRoll(List.of(4, 5, 5, 5, 5)), FOURS));
        assertEquals(0, yatzy.score(buildRoll(List.of(6, 3, 2, 5, 1)), FOURS));
    }

    @Test
    void should_test_category_fives() {
        assertEquals(10, yatzy.score(buildRoll(List.of(4, 4, 4, 5, 5)), FIVES));
        assertEquals(15, yatzy.score(buildRoll(List.of(4, 4, 5, 5, 5)), FIVES));
        assertEquals(20, yatzy.score(buildRoll(List.of(4, 5, 5, 5, 5)), FIVES));
        assertEquals(0, yatzy.score(buildRoll(List.of(6, 3, 2, 1, 1)), FIVES));
    }

    @Test
    void should_test_category_sixes() {
        assertEquals(0, yatzy.score(buildRoll(List.of(4, 4, 4, 5, 5)), SIXES));
        assertEquals(6, yatzy.score(buildRoll(List.of(4, 4, 6, 5, 5)), SIXES));
        assertEquals(18, yatzy.score(buildRoll(List.of(6, 5, 6, 6, 5)), SIXES));
    }

    @Test
    void should_test_category_one_pair() {
        assertEquals(6, yatzy.score(buildRoll(List.of(3, 4, 3, 5, 6)), ONE_PAIR));
        assertEquals(10, yatzy.score(buildRoll(List.of(5, 3, 3, 3, 5)), ONE_PAIR));
        assertEquals(12, yatzy.score(buildRoll(List.of(5, 3, 6, 6, 5)), ONE_PAIR));
        assertEquals(0, yatzy.score(buildRoll(List.of(5, 3, 6, 1, 2)), ONE_PAIR));
    }

    @Test
    void should_test_category_two_pairs() {
        assertEquals(16, yatzy.score(buildRoll(List.of(3, 3, 5, 4, 5)), TWO_PAIRS));
        assertEquals(16, yatzy.score(buildRoll(List.of(3, 3, 5, 5, 5)), TWO_PAIRS));
        assertEquals(0, yatzy.score(buildRoll(List.of(3, 1, 5, 5, 5)), TWO_PAIRS));
        assertEquals(0, yatzy.score(buildRoll(List.of(1, 2, 3, 4, 5)), TWO_PAIRS));
    }

    @Test
    void should_test_category_three_of_a_kind() {
        assertEquals(9, yatzy.score(buildRoll(List.of(3, 3, 3, 4, 5)), THREE_OF_A_KIND));
        assertEquals(15, yatzy.score(buildRoll(List.of(5, 3, 5, 4, 5)), THREE_OF_A_KIND));
        assertEquals(9, yatzy.score(buildRoll(List.of(3, 3, 3, 3, 5)), THREE_OF_A_KIND));
        assertEquals(0, yatzy.score(buildRoll(List.of(3, 4, 3, 2, 5)), THREE_OF_A_KIND));
    }

    @Test
    void should_test_category_four_of_a_kind() {
        assertEquals(12, yatzy.score(buildRoll(List.of(3, 3, 3, 3, 5)), FOUR_OF_A_KIND));
        assertEquals(20, yatzy.score(buildRoll(List.of(5, 5, 5, 4, 5)), FOUR_OF_A_KIND));
        assertEquals(12, yatzy.score(buildRoll(List.of(3, 3, 3, 3, 3)), FOUR_OF_A_KIND));
        assertEquals(0, yatzy.score(buildRoll(List.of(3, 2, 3, 4, 3)), FOUR_OF_A_KIND));
    }

    @Test
    void should_test_category_small_straight() {
        assertEquals(15, yatzy.score(buildRoll(List.of(1, 2, 3, 4, 5)), SMALL_STRAIGHT));
        assertEquals(15, yatzy.score(buildRoll(List.of(2, 3, 4, 5, 1)), SMALL_STRAIGHT));
        assertEquals(0, yatzy.score(buildRoll(List.of(1, 2, 2, 4, 5)), SMALL_STRAIGHT));
    }

    @Test
    void should_test_category_large_straight() {
        assertEquals(20, yatzy.score(buildRoll(List.of(6, 2, 3, 4, 5)), LARGE_STRAIGHT));
        assertEquals(20, yatzy.score(buildRoll(List.of(2, 3, 4, 5, 6)), LARGE_STRAIGHT));
        assertEquals(0, yatzy.score(buildRoll(List.of(1, 2, 2, 4, 5)), LARGE_STRAIGHT));
    }

    @Test
    void should_test_category_full_house() {
        assertEquals(18, yatzy.score(buildRoll(List.of(6, 2, 2, 2, 6)), FULL_HOUSE));
        assertEquals(0, yatzy.score(buildRoll(List.of(2, 3, 4, 5, 6)), FULL_HOUSE));
    }

    private static Roll buildRoll(List<Integer> dice) {
        return new Roll(dice);
    }
}
